// A "package" in Java is used to group related classes. Think of it as a folder in directory
    //Packages are divided into two categories
        //1. Built-in Packages - (packages from JAVA API)
        //2. User-defined Packages

//Package creation follow the "reverse domain name notation" for the naming convention
package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {

        //Variables
        // Syntax: dataType identifier;
        int age;
        char middleInitial;

        //Variable Declaration vs. Initialization
        int x;
        int y = 0;

        //Initialization after declaration
        x = 1;

        //output to the systems
        System.out.println("The value of y is " + y + " and the value of x is " + x);

        //Primitive Data Types
        //predefined within the Java programming which is used for a "single-valued" variables with limited capabilities

        //int - whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        //long
        //L is being added at the end of the long number to be recognized
        long worldPopulation = 7865154148556L;
        System.out.println(worldPopulation);

        //float
        //add f at the end of the float to be recognized
        float piFloat = 3.14159265359f;
        System.out.println(piFloat);

        //double
        double piDouble = 3.14159265359;
        System.out.println(piDouble);

        //char - single character
        //uses single quote
        char letter = 'a';
        System.out.println(letter);

        //boolean  true or false
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        //isTaken = true;

        //Constants
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);
        //PRINCIPAL = 4000;


        //Non-primitive data type
            //also known as reference data types refer to instances or objects

        //String
        //Stores a sequences or array of characters
        //String are actually object that can use methods

        String userName = "JSmith";
        System.out.println(userName);

        int stringLength = userName.length();
        System.out.println(stringLength);



    }
}
