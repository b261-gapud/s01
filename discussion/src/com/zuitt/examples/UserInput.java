package com.zuitt.example;

import java.util.Scanner;
public class UserInput {
    public static void main(String[] args) {
        // We instantiate the myObj from the Scanner class
        //Scanner is used for obtaining input from the terminal.
        //"System.in" allows us to take input from the console
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter Username:");

        //To capture the input given by the user, we use the nextLine() method
        // takes an input until the line change or new line and ends input of gets enter
        String userName = myObj.nextLine();
        System.out.println("Username is: " + userName);

        //String sample = "123username";
    }
}
