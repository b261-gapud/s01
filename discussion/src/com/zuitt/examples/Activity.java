package com.zuitt.examples;

import java.util.Scanner;
public class Activity {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = s.nextLine();

        System.out.println("Last Name:");
        String lastName = s.nextLine();

        System.out.println("First Subject Grade:");
        double firstSubjectGrade = s.nextDouble();

        System.out.println("Second Subject Grade:");
        double secondSubjectGrade = s.nextDouble();

        System.out.println("Third Subject Grade:");
        double thirdSubjectGrade = s.nextDouble();

        System.out.println();

        System.out.println("Good day, " + firstName + " " + lastName + ". \nYour average is: " + (firstSubjectGrade + secondSubjectGrade + thirdSubjectGrade)/3);


    }
}
