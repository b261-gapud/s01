// Main Class
// Serves as entry point for our Java program
// main class has 1 method inside, the main method => to run our code.

public class Main {
    //public - access modifier which simply tells the application which classes have access to methods/attributes.
    // static - keyword associated with a method/property that is related in a class. This will allow a method to be invoked without instantiating a class.
    // void - a keyword that is used to specify a method that doesn't return anything. In java we have to declare the data type of the method's return.
    // main() - is the name of the main method in Java. It is the entry point for the java program.
    public static void main(String[] args) {
        //to print statement in the terminal.
        System.out.println("Hello world!");
    }
}
